# XYGravity1960

# 개발 정보

### 개발 기간
2019/ 10/ 07 ~ 2019/ 10/ 27

### 개발 툴, 언어
Unity, C#

### 타겟 플랫폼
Android

### 개발 인원
- **기획, 그래픽, 레벨 디자인, 클라이언트 개발 1명 (본인)**

---

# 게임 소개
### 장르
2D, 퍼즐
### 스토리
- **플레이어** – 실험 지원자
- **게임 목표** – 방안의 중력관련 퍼즐들을 풀어 탈출 하는 것

# 게임 특징
- 중력을 사용하는 새로운 형식의 퍼즐 게임
- 향수를 자극하는 캐릭터와 캐주얼한 도트 그래픽
- 다양한 퍼즐요소

# 플레이 영상
[![](http://img.youtube.com/vi/yAeaxY2bxnw/0.jpg)](http://www.youtube.com/watch?v=yAeaxY2bxnw "")
[![](http://img.youtube.com/vi/yByiAkRI3og/0.jpg)](http://www.youtube.com/watch?v=yByiAkRI3og "")

<!--
# 컨텐츠 설명
## 적
## 동료
## 플레이어
-->
