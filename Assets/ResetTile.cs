﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetTile : ElectricDevice
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (on)
        {

            if (collision.GetComponent<BreakLimitCheck>())
            {
                Destroy(collision.GetComponent<BreakLimitCheck>().body.gameObject);
                collision.GetComponent<BreakLimitCheck>().body.BreakParticles();
            }
            else if(collision.GetComponent<G_Object>())
            {
                Destroy(collision.gameObject);
                collision.GetComponent<G_Object>().BreakParticles();
            }
        }

    }
}
