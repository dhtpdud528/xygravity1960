﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpController : MonoBehaviour
{
    PlayerController player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameManager.instance.playerController;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        player.floor = collision.transform;
        player.isFloor = true;

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        player.isFloor = false;
    }
}
