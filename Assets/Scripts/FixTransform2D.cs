﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixTransform2D : MonoBehaviour
{
    public Transform target;
    public enum FixType {Position,Rotation, Both }
    public FixType fixType;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch(fixType)
        {
            case FixType.Position: transform.position = new Vector2(target.position.x, target.position.y); break;
            case FixType.Rotation: transform.rotation = target.rotation; break;
            case FixType.Both:
                transform.position = new Vector2(target.position.x, target.position.y);
                transform.rotation = target.rotation;
                break;
        }
        
    }
}
