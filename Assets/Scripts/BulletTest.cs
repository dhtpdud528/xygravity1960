﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTest : MonoBehaviour
{
    public Vector2 dir;
    public bool local;
    public float torqueVelocity;
    public float maxVelocity;
    public enum SpeedType { Impulse, acceleration }
    public SpeedType speedType;
    // Start is called before the first frame update
    void Start()
    {
        if (speedType == SpeedType.Impulse)
        {
            if (local)
            {
                Quaternion qRotate = Quaternion.AngleAxis(transform.rotation.eulerAngles.z, Vector3.forward);
                Vector2 localVelocity = qRotate * dir;
                GetComponent<Rigidbody2D>().velocity += localVelocity;
            }
            else
                GetComponent<Rigidbody2D>().velocity += dir;
            GetComponent<Rigidbody2D>().angularVelocity += torqueVelocity;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (speedType == SpeedType.acceleration)
        {
            if (GetComponent<Rigidbody2D>().velocity.magnitude < maxVelocity)
                if (local)
                {
                    Quaternion qRotate = Quaternion.AngleAxis(transform.rotation.eulerAngles.z, Vector3.forward);
                    Vector2 localVelocity = qRotate * dir;
                    GetComponent<Rigidbody2D>().velocity += localVelocity * Time.deltaTime;
                }
                else
                    GetComponent<Rigidbody2D>().velocity += dir * Time.deltaTime;
            if (GetComponent<Rigidbody2D>().angularVelocity < maxVelocity)
                GetComponent<Rigidbody2D>().angularVelocity += torqueVelocity;
        }
    }
}
