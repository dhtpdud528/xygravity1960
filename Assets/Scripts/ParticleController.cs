﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    ParticleSystem ps;
    // Start is called before the first frame update
    void Start()
    {
        ps = GetComponent<ParticleSystem>();

        for(int i =0;i< GameManager.instance.g_Objects.Count;i++)
            ps.trigger.SetCollider(i,GameManager.instance.g_Objects[i].transform.GetChild(0));
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnParticleCollision(GameObject other)
    {
        List<ParticleSystem.Particle> inside = new List<ParticleSystem.Particle>();
        int numInside = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Inside, inside);
        for (int i = 0; i < numInside; i++)
        {
            ParticleSystem.Particle particle = inside[i];
            if (other.GetComponent<G_Zone>() != null)
                if (other.GetComponent<G_Zone>().body.gPower > 0)
                    particle.color = GameManager.instance.gColor;
                else if (other.GetComponent<G_Zone>().body.gPower < 0)
                    particle.color = GameManager.instance.antiGColor;
            inside[i] = particle;
        }
        ps.SetTriggerParticles(ParticleSystemTriggerEventType.Inside, inside);
    }
}
