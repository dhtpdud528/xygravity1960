﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerController : MonoBehaviour
{

    public GameManager gameManager;
    public AudioClip jumpSound;
    public AudioClip deadSound;
    public AudioClip grabSound;
    public Transform renderTransform;
    public float acceleration;
    public Rigidbody2D playerRigidbody;
    public PolygonCollider2D playerCollider;
    public JumpController jumpController;
    PhysicsMaterial2D playerSharedMaterialOrigin;
    public float maxSpeed;
    public float maxGrabDistance;
    public Vector2 HorizontalAcceleration;
    public bool isFloor;
    public Transform floor;
    public G_Object handOBJ;

    public bool isSettingGyro;
    public float jumpPower;

    public Vector2 preVelocity;
    public Vector2 curVelocity;
    public Vector2 vector2Velocity;

    Vector2 curStepDistance;
    Vector2 nextStepDistance;
    Vector2 handJoystickDir;

    private bool isJumpStart;
    bool viewMod;
    public bool isViewMod
    {
        set
        {
            //if(value && !gameManager.isMouseOverUI)
            viewMod = value;
        }

        get { return viewMod; }
    }
    public bool isPickUpMod;

    public Vector2 floorPoint;
    private bool attachedFloor;
    private Vector2 frictionVector;

    public Animator bodyAnim;
    public Animator handAnim;
    public Animator pickupHandAnim;
    private int dir;
    Vector2 handJoystickVector;
    Quaternion playerRotationZ;
    private bool isFixPickUpMod;

    private void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerCollider = GetComponent<PolygonCollider2D>();
        //playerSharedMaterialOrigin = playerCollider.sharedMaterial;

    }
    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(VelocityRecord());
        gameManager = GameManager.instance;
        jumpController = gameManager.jumpController;
        renderTransform = gameManager.CVC.m_Follow;
    }
    IEnumerator VelocityRecord()
    {
        while (true)
        {
            preVelocity = playerRigidbody.velocity;
            yield return new WaitForEndOfFrame();
            curVelocity = playerRigidbody.velocity;
        }

    }
    public void Die()
    {
        StartCoroutine(Dead());
    }
    IEnumerator Dead()
    {
        if(deadSound != null)
            gameManager.audioSource.PlayOneShot(deadSound);
        gameManager.PlayerDead();
        bodyAnim.SetBool("Dead", true);
        handAnim.GetComponent<SpriteRenderer>().enabled = false;
        pickupHandAnim.GetComponent<SpriteRenderer>().enabled = false;
        //playerCollider.size = new Vector2(0.32f, 0.16f);
        yield return new WaitForSecondsRealtime(3);
        //gameManager.ReStart();
    }
    // Update is called once per frame
    void Update()
    {
        if (bodyAnim.GetBool("Dead")) return;
        handJoystickVector = new Vector2(gameManager.handJoystick.Horizontal, gameManager.handJoystick.Vertical);
        playerRotationZ = Quaternion.AngleAxis(transform.rotation.eulerAngles.z, Vector3.forward);
        bodyAnim.SetBool("IsFloor", isFloor);
        handAnim.SetBool("IsFloor", isFloor);
        pickupHandAnim.SetBool("IsFloor", isFloor);
        if (gameManager.IsJoyStick())
            isViewMod = false;


        if (Input.GetButtonDown("Jump"))
            Jump();

        //if (isFloor)
        //{
        //    if (!isJumpStart)
        //        playerRigidbody.velocity = newVelocity;
        //}
        //else
        //    isJumpStart = false;
        if (handJoystickVector.magnitude > 0)
        {
            isPickUpMod = true;
            if (handJoystickVector.magnitude < 1)
                isFixPickUpMod = false;
            else
                isFixPickUpMod = true;
        }
        else if (!isFixPickUpMod)
            isPickUpMod = false;

        if (Input.GetKeyDown(KeyCode.E))
        {
            isPickUpMod = !isPickUpMod;
            if (handOBJ != null)
            {
                ReleaseHand();
            }
        }


        if (isPickUpMod)
        {
            handAnim.GetComponent<SpriteRenderer>().enabled = false;
            pickupHandAnim.GetComponent<SpriteRenderer>().enabled = true;
            int layerMask = (1 << 12);
            Vector2 playerpos = new Vector2(transform.position.x, transform.position.y);
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //(touchPos - playerpos).normalized;
            if (handJoystickVector.magnitude > 0)
            {
                handJoystickDir = playerRotationZ * handJoystickVector.normalized;
                //Debug.DrawRay(transform.position, handJoystickDir * maxGrabDistance, Color.yellow);
            }

            RaycastHit2D hit = Physics2D.Raycast(transform.position, handJoystickDir, maxGrabDistance, layerMask);

            int layerMask2 = (1 << 0) + (1 << 12);
            RaycastHit2D rayPos = Physics2D.Raycast(transform.position, handJoystickDir, maxGrabDistance, layerMask2);


            Vector2 handpos = playerpos + (handJoystickDir * maxGrabDistance);

            if (handOBJ != null)
            {
                handOBJ.transform.rotation = transform.rotation;
                handOBJ.objRigidbody.velocity *= 0;
                handOBJ.objRigidbody.angularVelocity *= 0;
                handOBJ.objCollider.enabled = false;
                handOBJ.floorCollider.enabled = false;
                //handOBJ.limitCollider.enabled = false;
                if (rayPos && rayPos.collider.gameObject != handOBJ.gameObject)
                    handOBJ.transform.position = rayPos.point;
                else
                    handOBJ.transform.position = handpos;
            }
            else
            {
                if (hit && hit.collider.GetComponent<G_Object>() != null && hit.transform.localScale.x == 1 && hit.collider.GetComponent<G_Object>().gPower == 0)
                {
                    handOBJ = hit.collider.GetComponent<G_Object>();
                    gameManager.audioSource.PlayOneShot(grabSound);
                }
            }
        }
        else
        {
            handAnim.GetComponent<SpriteRenderer>().enabled = true;
            pickupHandAnim.GetComponent<SpriteRenderer>().enabled = false;
            if (handOBJ != null)
            {
                ReleaseHand();
            }
        }


        if (isViewMod)
        {
            gameManager.CVC.m_Follow = null;
            gameManager.CVC.m_LookAt = null;
        }
        else
        {
            if (gameManager.CVC.m_Follow == null)
                gameManager.CVC.m_Follow = renderTransform;
            //gameManager.CVC.m_LookAt = transform;
            //float eulerZ = transform.rotation.eulerAngles.z < 180 ? transform.rotation.eulerAngles.z
            if (!isSettingGyro)
                gameManager.CVC.transform.rotation = Quaternion.Slerp(gameManager.CVC.transform.rotation, transform.rotation, 10 * Time.deltaTime);
            //float MaxOffsetX = playerRigidbody.velocity.x > 0 ? gameManager.screenWidthRatio : -gameManager.screenWidthRatio;
            //float MaxOffsetY = playerRigidbody.velocity.y > 0 ? gameManager.screenHeightRatio : -gameManager.screenHeightRatio;

            //gameManager.CVC.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset = Vector3.Slerp(gameManager.CVC.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset,
            //    new Vector3(
            //        Mathf.Abs(playerRigidbody.velocity.x) > gameManager.screenWidthRatio ? MaxOffsetX : playerRigidbody.velocity.x,
            //        Mathf.Abs(playerRigidbody.velocity.y) > gameManager.screenHeightRatio ? MaxOffsetY : playerRigidbody.velocity.y,
            //        -10)
            //    ,5*Time.deltaTime);
        }
        if (isSettingGyro)
        {
            Vector2 direction = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //Debug.Log(Vector2.SignedAngle(direction, -Camera.main.transform.right));
            transform.up = direction.normalized;
            playerRigidbody.angularVelocity = 0;
            isViewMod = false;
        }
        //gameManager.CVC.transform.position = new Vector3(gameManager.CVC.transform.position.x, gameManager.CVC.transform.position.y,-10);

    }
    public void ReleaseHand()
    {
        handOBJ.objCollider.enabled = true;
        handOBJ.floorCollider.enabled = true;
        //handOBJ.limitCollider.enabled = true;
        handOBJ.objRigidbody.velocity = playerRigidbody.velocity;
        handOBJ.objRigidbody.angularVelocity *= 0;
        handOBJ = null;
    }
    public void Jump()
    {
        if (bodyAnim.GetBool("Dead")) return;
        isJumpStart = true;
        if (isFloor)
        {
            if (jumpSound != null)
                gameManager.audioSource.PlayOneShot(jumpSound);
            float reaction = floor.GetComponent<Rigidbody2D>() != null && floor.GetComponent<Rigidbody2D>().bodyType == RigidbodyType2D.Dynamic &&
                floor.GetComponent<Rigidbody2D>().mass < playerRigidbody.mass ?
                floor.GetComponent<Rigidbody2D>().mass : playerRigidbody.mass;

            if(floor.GetComponent<Rigidbody2D>() != null && floor.GetComponent<Rigidbody2D>().bodyType == RigidbodyType2D.Dynamic)
                floor.GetComponent<Rigidbody2D>().AddForce(-transform.up * jumpPower * reaction, ForceMode2D.Impulse);
            playerRigidbody.AddForce(transform.up * jumpPower * reaction, ForceMode2D.Impulse);
        }
    }
    protected void FixedUpdate()
    {
        if (bodyAnim.GetBool("Dead")) return;
        float xSpeed = acceleration * (gameManager.moveJoystick.Horizontal + Input.GetAxis("Horizontal"));
        bodyAnim.SetFloat("Speed", Mathf.Abs(xSpeed));
        handAnim.SetFloat("Speed", Mathf.Abs(xSpeed));
        pickupHandAnim.SetFloat("Speed", Mathf.Abs(xSpeed));

        Vector2 direction = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (handJoystickVector.magnitude > 0)
        {
            direction = playerRotationZ * -handJoystickVector;
        }

        if (xSpeed != 0)
        {
            dir = (xSpeed < 0 ? 1 : -1);
            bodyAnim.transform.localScale = xSpeed < 0 ? new Vector2(-1, 1) : new Vector2(1, 1);
            handAnim.GetComponent<SpriteRenderer>().flipX = xSpeed < 0 ? true : false;
            //pickupHandAnim.GetComponent<SpriteRenderer>().flipX = xSpeed < 0 ? true : false;
        }

        //손 방향
        if (handJoystickVector.magnitude > 0)
            pickupHandAnim.transform.right = dir * direction.normalized;
        HorizontalAcceleration = xSpeed * transform.right * Time.deltaTime;
        if (isFloor)
        {
            if (!isJumpStart)
            {
                Vector2 floorOBJVelocty = floor.GetComponent<Rigidbody2D>() != null ? floor.GetComponent<Rigidbody2D>().velocity : Vector2.zero;
                Vector2 subjectiveVelocity = (playerRigidbody.velocity) - (floorOBJVelocty + this.frictionVector);

                if (xSpeed == 0)
                    playerRigidbody.velocity -= subjectiveVelocity * Time.deltaTime * 2;

                if ((subjectiveVelocity + HorizontalAcceleration).magnitude < maxSpeed)
                    playerRigidbody.velocity += HorizontalAcceleration;
            }
        }
        else
            isJumpStart = false;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        attachedFloor = true;
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        frictionVector = collision.relativeVelocity;
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        attachedFloor = false;
    }
}
