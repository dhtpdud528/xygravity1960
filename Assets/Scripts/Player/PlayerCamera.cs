﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;

public class PlayerCamera : MonoBehaviour
{
    GameManager gameManager;
    LineRenderer dragLine;
    Vector3 touchStart;
    public Vector2 updateMousePos;
    public float zoomOutMin = 1;
    public float zoomOutMax = 8;
    public float MaxOffsetX = 1.6f;
    public float MaxOffsetY = .9f;

    IEnumerator viewModCoroutine;

    public G_Object clickObj;
    float clickedPower = 0;
    public bool isMassMod;
    public bool isDotMod;

    int layerMask = (1 << 2) + (1 << 9) + (1 << 11);
    private int dragDirNormalize;
    public bool preMoseOverUI;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.instance;
        dragLine = gameManager.dragLine;
    }
    IEnumerator ViewMod()
    {
        yield return new WaitForSeconds(3);
        EndViewMod();
    }
    public void EndViewMod()
    {
        gameManager.playerController.isViewMod = false;
        if (gameManager.CVC.m_Follow != gameManager.playerController.renderTransform)
            gameManager.CVC.m_Follow = gameManager.playerController.renderTransform;
        //gameManager.CVC.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset = new Vector3(0, 0, -10);
    }

    // Update is called once per frame
    void Update()
    {
        zoom(Input.GetAxis("Mouse ScrollWheel") * 5);
        if (!gameManager.playerController.isViewMod && !gameManager.playerController.isSettingGyro)
            if (gameManager.IsJoyStick() || gameManager.isMouseOverUI && !isMassMod)
            {
                touchStart = Vector3.zero;
                updateMousePos = Vector3.zero;
                gameManager.CVC.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset
                        = Vector3.Lerp(gameManager.CVC.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset,
                        new Vector3(gameManager.playerController.playerRigidbody.velocity.x/2, gameManager.playerController.playerRigidbody.velocity.y/2, -10), 2 * Time.deltaTime);
                return;
            }
            else
            {
                gameManager.CVC.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset
                            = Vector3.Lerp(gameManager.CVC.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset,
                            new Vector3(0, 0, -10), 2 * Time.fixedUnscaledDeltaTime);
            }
        //"터치 한번만 했을 때" 조건에 넣기
        if (!gameManager.isMouseOverUI && Input.touchCount >= 2)
        {
            touchStart = Vector3.zero;
            DoViewMod();

            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);

            Vector2 touch1PrevPos = touch1.position - touch1.deltaPosition;
            Vector2 touch2PrevPos = touch2.position - touch2.deltaPosition;

            Vector2 touchPrevVector = touch1PrevPos - touch2PrevPos;
            Vector2 touchVector = touch1.position - touch2.position;

            float prevMagnitude = touchPrevVector.magnitude;
            float currentMagnitude = touchVector.magnitude;

            float difference = currentMagnitude - prevMagnitude;

            zoom(difference * 0.01f);

            if (!gameManager.playerController.isSettingGyro)
                gameManager.CVC.transform.rotation =
                    Quaternion.Euler(
                        gameManager.CVC.transform.rotation.eulerAngles.x,
                        gameManager.CVC.transform.rotation.eulerAngles.y,
                        gameManager.CVC.transform.rotation.eulerAngles.z - Vector2.SignedAngle(touchPrevVector, touchVector));
        }
        else 
            if (Input.GetMouseButtonDown(0))
            {
                if (gameManager.isMouseOverUI)
                    preMoseOverUI = true;
                else
                    preMoseOverUI = false;
                touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                updateMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                RaycastHit2D hit = Physics2D.Raycast(touchStart, transform.forward, Mathf.Infinity, (1 << 12) + (1 << 8));

                if (!preMoseOverUI)
                    gameManager.playerController.isViewMod = true;
                if (hit && hit.collider.GetComponent<G_Object>() != null && !gameManager.playerController.isSettingGyro && gameManager.canClickOBJ)
                {
                    isMassMod = true;
                    isDotMod = true;
                    clickObj = hit.collider.GetComponent<G_Object>();
                    dragLine.enabled = true;


                    //obj.ChangeMass(obj.massCount + 1);
                }
                else if (hit && hit.collider.GetComponent<PlayerController>() != null)
                {
                    gameManager.playerController.isSettingGyro = true;
                    gameManager.playerController.isViewMod = false;
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                CompleteAbility();
            }
            else if (Input.GetMouseButton(0) && !preMoseOverUI)
            {
                updateMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (!gameManager.playerController.isViewMod && !isMassMod)
                {
                    Vector2 screenCenter = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2));
                    Vector2 camPos = updateMousePos - screenCenter;

                    float offsetX = 0;
                    float offsetY = 0;

                    if (camPos.x != 0)
                        offsetX = Mathf.Abs(camPos.x) >= MaxOffsetX ? (camPos.x > 0 ? MaxOffsetX : -MaxOffsetX) : camPos.x;
                    if (camPos.y != 0)
                        offsetY = Mathf.Abs(camPos.y) >= MaxOffsetY ? (camPos.y > 0 ? MaxOffsetY : -MaxOffsetY) : camPos.y;

                    gameManager.CVC.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset
                        = Vector3.Lerp(gameManager.CVC.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset,
                        new Vector3(offsetX, offsetY, -10), 2 * Time.fixedUnscaledDeltaTime);
                }
                else if (!gameManager.IsJoyStick() && !isMassMod && !gameManager.playerController.isSettingGyro && !Input.GetKey(KeyCode.LeftControl))
                {
                //gameManager.CVC.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset = new Vector3(0,0,-10);
                DoViewMod();
                    if (touchStart != Vector3.zero)
                    {
                        Vector3 direction = touchStart - Camera.main.ScreenToWorldPoint(Input.mousePosition);
                        gameManager.CVC.transform.position += direction;
                    }

                }
            }
        
        if (clickObj != null)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                CompleteAbility();
            }
            if (isMassMod)
            {
                Vector2 clickobjPos = clickObj.transform.position;
                Vector2 dragDir = updateMousePos - clickobjPos;
                float dragDis = Vector2.Distance(updateMousePos, clickobjPos);
                DoViewMod();
                MoveOffset(updateMousePos);
                
                if (isDotMod)
                {
                    
                    dragDirNormalize = Vector2.Dot(dragDir.normalized, transform.right.normalized) > 0 ? 1 : -1;
                    if (dragDis > 0.2f * gameManager.CVC.m_Lens.OrthographicSize)
                    {
                        //gameManager.CVC.m_Follow = clickObj.transform;
                        //gameManager.playerController.isViewMod = false;
                        isDotMod = false;
                    }
                }
                if (dragDis > 0.05f)
                    clickedPower = dragDirNormalize * dragDis;
                else
                    clickedPower = 0;

                SetSizeTemp(clickObj, clickedPower);

                dragLine.material.color = dragDirNormalize > 0 ? gameManager.gColor : gameManager.antiGColor;
                dragLine.SetPosition(0, clickobjPos);
                dragLine.SetPosition(1, updateMousePos);
            }
            else
            {
                dragLine.enabled = false;
                if (clickObj != null)
                    clickObj.ChangeMass(clickedPower);
                clickedPower = 0;
                clickObj = null;
            }
        }
        //else if (Input.GetMouseButtonDown(1))
        //{
        //    touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //    RaycastHit2D hit = Physics2D.Raycast(touchStart, transform.forward, Mathf.Infinity, ~layerMask);
        //    if (hit && hit.collider.GetComponent<G_Object>() != null)
        //    {
        //        G_Object obj = hit.collider.GetComponent<G_Object>();
        //        obj.ChangeMass(obj.massCount - 1);
        //    }
        //}

        //else if (Input.GetMouseButton(2) && Input.GetKey(KeyCode.LeftControl))
        //{
        //    Vector2 direction = Camera.main.transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //    Vector2 touchStartVector = Camera.main.transform.position - touchStart;

        //    if (!gameManager.playerController.isSettingGyro)
        //        gameManager.CVC.transform.rotation = Quaternion.Euler(gameManager.CVC.transform.rotation.x, gameManager.CVC.transform.rotation.y, Vector2.SignedAngle(direction, -Camera.main.transform.right));

        //}

    }
    public void SetSizeTemp(G_Object obj, float power)
    {
        if(obj.GetComponent<ObjectCube>())
        {
            gameManager.CubeSizeTemps[0].gameObject.SetActive(true);
            gameManager.CubeSizeTemps[1].gameObject.SetActive(true);
            float size = power != 0 ? (Mathf.Abs(power) * 100) / (obj.GetComponent<SpriteRenderer>().size.x * 100) * 2 : 1;
            gameManager.CubeSizeTemps[0].transform.position = obj.transform.position;
            gameManager.CubeSizeTemps[1].transform.position = obj.transform.position;
            gameManager.CubeSizeTemps[0].transform.localScale = new Vector3(size, obj.transform.lossyScale.y, obj.transform.lossyScale.z);
            gameManager.CubeSizeTemps[1].transform.localScale = new Vector3(obj.transform.lossyScale.x, size, obj.transform.lossyScale.z);
        }
        else if(obj.GetComponent<ObjectSphere>())
        {
            gameManager.SphreSizeTemps.gameObject.SetActive(true);
            float size = power != 0 ? (Mathf.Abs(power) * 100) / (obj.GetComponent<CircleCollider2D>().radius * 100) : 1;
            gameManager.SphreSizeTemps.transform.position = obj.transform.position;
            gameManager.SphreSizeTemps.transform.localScale = new Vector3(size, size, obj.gameManager.SphreSizeTemps.transform.lossyScale.z);
        }
        
    }
    public void MoveOffset(Vector3 campos)
    {
        
        float power = 1f;
        Vector2 screenCenter = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2));
        Vector2 screenHorizonLimit = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 4 * 3, Screen.height / 2));
        Vector2 screenVerticalLimit = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 4 * 3));

        float limitY = Mathf.Abs(screenCenter.y - screenVerticalLimit.y);
        float limitX = Mathf.Abs(screenCenter.x - screenHorizonLimit.x);

        if (campos.y > screenCenter.y + limitY)
        {
            gameManager.CVC.transform.position += new Vector3(0, Mathf.Abs(campos.y - screenCenter.y) * Time.deltaTime * power, 0);
        }
        if (campos.y < screenCenter.y - limitY)
        {
            gameManager.CVC.transform.position += new Vector3(0, -Mathf.Abs(campos.y - screenCenter.y) * Time.deltaTime * power, 0);
        }
        if (campos.x > screenCenter.x + limitX)
        {
            gameManager.CVC.transform.position += new Vector3(Mathf.Abs(campos.x - screenCenter.x) * Time.deltaTime * power, 0, 0);
        }
        if (campos.x < screenCenter.x - limitX)
        {
            gameManager.CVC.transform.position += new Vector3(-Mathf.Abs(campos.x - screenCenter.x) * Time.deltaTime * power, 0, 0);
        }
    }

    public void DoViewMod()
    {
        gameManager.CVC.m_Follow = null;
        if (viewModCoroutine != null)
            StopCoroutine(viewModCoroutine);
        gameManager.playerController.isViewMod = true;
        StartCoroutine(viewModCoroutine = ViewMod());
    }
    public void CompleteAbility()
    {
        gameManager.CubeSizeTemps[0].gameObject.SetActive(false);
        gameManager.CubeSizeTemps[1].gameObject.SetActive(false);
        gameManager.SphreSizeTemps.gameObject.SetActive(false);
        if (!gameManager.playerController.isViewMod && gameManager.CVC.m_Follow != gameManager.playerController.renderTransform)
            gameManager.CVC.m_Follow = gameManager.playerController.renderTransform;

        isMassMod = false;
        if (gameManager.playerController.isSettingGyro)
        {
            //EndViewMod();
            gameManager.playerController.isSettingGyro = false;
        }
        //else if (!preMoseOverUI)
        //{
        //    gameManager.playerController.renderTransform.GetComponent<SpriteRenderer>().material.color = Color.red;
        //    DoViewMod();
        //}
        //clickObj = null;
    }
    void zoom(float increment)
    {
        gameManager.CVC.m_Lens.OrthographicSize = Mathf.Clamp(gameManager.CVC.m_Lens.OrthographicSize - increment, zoomOutMin, zoomOutMax);
        gameManager.dragLine.endWidth = gameManager.CVC.m_Lens.OrthographicSize;
    }
}
