﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedRair : MonoBehaviour
{
    public G_Object fixedOBJ;
    public Vector2[] lines = new Vector2[4];
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < lines.Length; i++)
        {
            lines[0] = new Vector2(0, 1).normalized;
            lines[1] = new Vector2(0, -1).normalized;
            lines[2] = new Vector2(-1, 0).normalized;
            lines[3] = new Vector2(1, 0).normalized;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
