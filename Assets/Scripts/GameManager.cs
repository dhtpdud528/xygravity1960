﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;
using Cinemachine;
using UnityEngine.EventSystems;
using UnityEngine.Playables;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public GameObject[] dontDestroyed;
    public AudioSource audioSource;
    public EventsList eventsList;
    public GameObject deadUI;
    public Image fade;

    EventSystem eventSystem;
    public PlayableDirector playableDirector;
    public VariableJoystick moveJoystick;
    public VariableJoystick handJoystick;
    public Button buttonJump;

    public GameObject objDetstoyParticles;
    public ParticleSystemForceField gravityZone;
    public PlayerCamera playerCam;
    public PlayerController playerController;
    public CinemachineVirtualCamera CVC;
    public JumpController jumpController;
    public List<Rigidbody2D> allOBJ = new List<Rigidbody2D>();

    public List<G_Zone> g_Objects = new List<G_Zone>();

    public Tilemap DefaultTilemap;
    public Tilemap ElectricTilemap;

    public int screenWidthRatio;
    public int screenHeightRatio;

    public Color gColor = new Color32(255, 147, 107, 128);
    public Color antiGColor = new Color32(107, 147, 255, 128);
    public Transform[] CubeSizeTemps;
    public Transform SphreSizeTemps;

    public Vector2 gravityDir;

    public LineRenderer dragLine;
    private bool isGamePause;

    IEnumerator pauseCoroutine;
    public bool isMouseOverUI;

    public bool canClickOBJ;

    public PhysicsMaterial2D DefaultFriction;
    public PhysicsMaterial2D ZeroFriction;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 게임 매니저가 존재합니다!");
            Destroy(gameObject);
        }
        if(dontDestroyed != null)
            for(int i=0;i<dontDestroyed.Length;i++)
                DontDestroyOnLoad(dontDestroyed[i]);
        audioSource = Camera.main.GetComponent<AudioSource>();
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f;
        eventsList = GetComponent<EventsList>();
        eventSystem = GetComponent<EventSystem>();
        Rigidbody2D[] rigidbody2Ds = FindObjectsOfType<Rigidbody2D>();
        for (int i = 0; i < rigidbody2Ds.Length; i++)
            if(rigidbody2Ds[i].bodyType == RigidbodyType2D.Dynamic)
                allOBJ.Add(rigidbody2Ds[i]);
        playerCam = FindObjectOfType<PlayerCamera>();
        playerController = FindObjectOfType<PlayerController>();
        CVC = FindObjectOfType<CinemachineVirtualCamera>();
        jumpController = FindObjectOfType<JumpController>();
        gravityDir = Vector2.zero;

        int screenGCD = GCD(Screen.width, Screen.height);
        screenWidthRatio = Screen.width / screenGCD;
        screenHeightRatio = Screen.height / screenGCD;

        InitGObjects();
        //Rigidbody2D[] objects = FindObjectsOfType<Rigidbody2D>();
        //for (int i = 0; i < FindObjectsOfType<Rigidbody2D>().Length; i++)
        //    objects[i].gravityScale = 0;
    }
    public void InitGObjects()
    {
        g_Objects.AddRange(FindObjectsOfType<G_Zone>());
    }
    // Start is called before the first frame update
    void Start()
    {
        if(eventsList.StartEvent != null)
            eventsList.StartEvent.Invoke();
    }
    public void ImageFadeOut(Text obj)
    {
        obj.raycastTarget = false;
        StartCoroutine(ImageFadeOutCoroutine(obj));
    }
    public void ImageFadeOut(Image obj)
    {
        obj.raycastTarget = false;
        StartCoroutine(ImageFadeOutCoroutine(obj));
    }
    IEnumerator ImageFadeOutCoroutine(Image obj)
    {
        obj.DOColor(new Color(1, 1, 1, 0), 1);
        yield return new WaitForSeconds(1);
    }
    IEnumerator ImageFadeOutCoroutine(Text obj)
    {
        obj.DOColor(new Color(1, 1, 1, 0), 1);
        yield return new WaitForSeconds(1);
    }
    public void MoveNextScene(int moveScene)
    {
        StartCoroutine(MoveSceneCoroutine(moveScene));
        
    }
    public void MoveScene(int moveScene)
    {
        SceneManager.LoadScene(moveScene);
    }

    IEnumerator MoveSceneCoroutine(int moveScene)
    {
        eventsList.FadeIn();
        yield return new WaitForSeconds(5);
        MoveScene(moveScene);
    }

    public bool IsJoyStick()
    {
        Vector2 handJoystickVector = new Vector2(handJoystick.Horizontal, handJoystick.Vertical);
        return moveJoystick.Horizontal != 0 || handJoystickVector.magnitude > 0;
    }

    // Update is called once per frame
    void Update()
    {
        isMouseOverUI = eventSystem.IsPointerOverGameObject();
    }
    private bool IsPointerOverUIObject()
    {
        for(int i=0;i<Input.touchCount;i++)
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.GetTouch(i).position.x, Input.GetTouch(i).position.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }
        return false;
    }
    public void PlayerDead()
    {
        deadUI.SetActive(true);
    }
    private void FixedUpdate()
    {
    }
    int GCD(int a, int b)
    {
        int Remainder;

        while (b != 0)
        {
            Remainder = a % b;
            a = b;
            b = Remainder;
        }
        return a;
    }
    public void GamePause()
    {
        isGamePause = !isGamePause;
        if(pauseCoroutine != null)
        {
            StopCoroutine(pauseCoroutine);
            pauseCoroutine = null;
        }
        StartCoroutine(pauseCoroutine = BulletTimeEvent());
    }
    public void ReStart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    private IEnumerator BulletTimeEvent()
    {
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        if (isGamePause)
        {
            while (Time.timeScale > 0.05f)
            {
                for (int i = 0; i < audioSources.Length; i++)
                    if (audioSources[i] != null)
                        audioSources[i].pitch *= Time.timeScale;
                Time.timeScale -= 0.05f;
                Time.fixedDeltaTime = 0.02f * Time.timeScale;
                yield return new WaitForSecondsRealtime(0.001f);
            }
            Time.timeScale = 0f;
            Time.fixedDeltaTime = 0f;
        }
        else
        {
            while (Time.timeScale < 1)
            {
                for (int i = 0; i < audioSources.Length; i++)
                    if (audioSources[i] != null)
                        audioSources[i].pitch *= Time.timeScale;
                Time.timeScale += 0.01f;
                Time.fixedDeltaTime = 0.02f * Time.timeScale;
                yield return new WaitForSecondsRealtime(0.01f);
            }
            for (int i = 0; i < audioSources.Length; i++)
                if (audioSources[i] != null)
                    audioSources[i].pitch = 1;
            Time.timeScale = 1;
            Time.fixedDeltaTime = 0.02f;
        }

        pauseCoroutine = null;
    }
}
