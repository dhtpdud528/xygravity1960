﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityCube : G_Zone
{
    public bool x;
    
    private void Update()
    {
        
    }
    public override void OnTriggerStay2D(Collider2D collision)
    {
        base.OnTriggerStay2D(collision);
        if (body.gPower == 0 || collision.attachedRigidbody == null || collision.attachedRigidbody.bodyType != RigidbodyType2D.Dynamic || collision.gameObject == body.gameObject || collision.gameObject.GetComponent<G_Zone>() != null) return;
        float gravity = -9.81f*Time.deltaTime* (body.gPower > 0 ? 1 : -1);
        Vector2 dir = collision.transform.position - body.transform.position;
        Quaternion qRotate = Quaternion.AngleAxis(-transform.rotation.eulerAngles.z, Vector3.forward);
        Vector2 v3Dest = qRotate * dir;
        Vector2 forceDir = Vector2.zero;

        if (x && v3Dest.x > body.GetComponent<SpriteRenderer>().size.x/2 || v3Dest.x < -body.GetComponent<SpriteRenderer>().size.x / 2)
            forceDir = v3Dest.x > 0 ? new Vector2(transform.right.x, transform.right.y).normalized * gravity : new Vector2(transform.right.x, transform.right.y).normalized * -gravity;
        else if(v3Dest.y > body.GetComponent<SpriteRenderer>().size.y / 2 || v3Dest.y < -body.GetComponent<SpriteRenderer>().size.y/ 2)
            forceDir = v3Dest.y > 0 ? new Vector2(transform.up.x, transform.up.y).normalized * gravity : new Vector2(transform.up.x, transform.up.y).normalized * -gravity;
        collision.attachedRigidbody.velocity += forceDir;



    }
}
