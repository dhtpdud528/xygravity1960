﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCube : G_Object
{
    public ParticleSystemForceField[] psffx;
    public ParticleSystemForceField[] psffy;
    public override void Start()
    {
        base.Start();
    }
    public override void ChangeMass(float power)
    {
        base.ChangeMass(power);
        float size = gPower != 0 ? (Mathf.Abs(gPower) * 100) / (GetComponent<SpriteRenderer>().size.x * 100) * 2 : 1;
        float val = 1;
        float mul = 9.81f*2f;
        GravityZones[0].transform.parent = null;
        GravityZones[1].transform.parent = null;
        GravityZones[0].transform.localScale = new Vector3(size, GravityZones[0].transform.localScale.y, GravityZones[0].transform.localScale.z);
        GravityZones[1].transform.localScale = new Vector3(GravityZones[1].transform.localScale.x, size, GravityZones[1].transform.localScale.z);
        GravityZones[0].transform.parent = transform;
        GravityZones[1].transform.parent = transform;

        psffx[0].directionX = (gPower > 0 ? val / GravityZones[0].transform.lossyScale.x * mul : -val / GravityZones[0].transform.lossyScale.x * mul);
        psffx[1].directionX = (gPower > 0 ? -val / GravityZones[0].transform.lossyScale.x * mul : val / GravityZones[0].transform.lossyScale.x * mul);

        psffy[0].directionY = (gPower < 0 ? val / GravityZones[1].transform.lossyScale.y * mul : -val / GravityZones[1].transform.lossyScale.y * mul);
        psffy[1].directionY = (gPower < 0 ? -val / GravityZones[1].transform.lossyScale.y * mul : val / GravityZones[1].transform.lossyScale.y * mul);
    }
}
