﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSphere : G_Object
{
    public ParticleSystemForceField psff;
    public override void Awake()
    {
        base.Awake();
    }
    public override void ChangeMass(float power)
    {
        base.ChangeMass(power);
        float size = gPower != 0 ? (Mathf.Abs(gPower)*100) / (GetComponent<CircleCollider2D>().radius*100) : 1;
        GravityZones[0].transform.parent = null;
        GravityZones[0].transform.localScale = new Vector3(size, size, GravityZones[0].transform.localScale.z);
        GravityZones[0].transform.parent = transform;
        psff.gravity = gPower > 0 ? 0.01f : -0.01f;
    }
    
}
