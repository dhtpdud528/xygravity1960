﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitySphere : G_Zone
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
    public override void OnTriggerStay2D(Collider2D collision)
    {
        base.OnTriggerStay2D(collision);
        if (body.gPower == 0 || collision.attachedRigidbody == null || collision.attachedRigidbody.bodyType != RigidbodyType2D.Dynamic || collision.gameObject == body.gameObject || collision.gameObject.GetComponent<G_Zone>() != null) return;
        float gravity = -9.81f * (body.gPower > 0 ? 1 : -1) * Time.deltaTime ;
        Vector2 dir = body.transform.position - collision.transform.position;
        Vector2 normalDir = new Vector2(dir.normalized.x, dir.normalized.y);
        collision.attachedRigidbody.velocity += -normalDir * gravity;
    }
}
