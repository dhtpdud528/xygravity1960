﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G_Zone : MonoBehaviour
{
    public G_Object body;
    public GameManager gameManager;
    public virtual void Start()
    {
        gameManager = GameManager.instance;
    }
    public void ChangeMassColor(float addMass)
    {
        if (addMass != 0)
            GetComponent<SpriteRenderer>().color = addMass > 0 ? GameManager.instance.gColor : GameManager.instance.antiGColor;
        else
            GetComponent<SpriteRenderer>().color = new Color();//투명
    }
    public virtual void OnTriggerStay2D(Collider2D collision)
    {
        
    }
}
