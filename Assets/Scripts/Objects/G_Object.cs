﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G_Object : MonoBehaviour
{
    public G_Zone[] GravityZones;
    public float gPower = 0;
    public Rigidbody2D objRigidbody;
    public GameManager gameManager;
    public Collider2D objCollider;
    public Collider2D floorCollider;
    public Collision2D objCollision;
    public Collider2D limitCollider;
    protected float massOrigin;

    public float maxMass = 50;

    IEnumerator massChanging;
    // Start is called before the first frame update
    public virtual void Awake()
    {
        objRigidbody = GetComponent<Rigidbody2D>();
        objCollider = GetComponent<Collider2D>();
        massOrigin = objRigidbody.mass;
    }
    public virtual void Start()
    {
        ChangeMass(gPower);
        gameManager = GameManager.instance;
        for (int i =0; i< GravityZones.Length;i++)
            GravityZones[i].body = this;
    }

    public void BreakParticles()
    {
        GameObject particles = Instantiate(gameManager.objDetstoyParticles, transform.position, Quaternion.identity);
        particles.transform.parent = gameManager.DefaultTilemap.transform;
    }

    // Update is called once per frame
    public virtual void Update()
    {
    }
    public virtual void ChangeMass(float power)
    {
        gPower = power;
        if (massChanging == null)
            StartCoroutine(massChanging = ChangeLerpMass(power != 0 ? 500 : massOrigin));
        else
        {
            StopCoroutine(massChanging);
            StartCoroutine(massChanging = ChangeLerpMass(power != 0 ? 500 : massOrigin));
        }
        for (int i = 0; i < GravityZones.Length; i++)
            GravityZones[i].ChangeMassColor(power);
    }
    IEnumerator ChangeLerpMass(float val)
    {
        while (Mathf.Abs(objRigidbody.mass - val) > 0.01f)
        {
            objRigidbody.mass = Mathf.Lerp(objRigidbody.mass, val, 5*Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        objRigidbody.mass = val;
        massChanging = null;
    }
    public void OnCollisionStay2D(Collision2D collision)
    {
        objCollision = collision;
    }
}
