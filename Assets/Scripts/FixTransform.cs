﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixTransform : MonoBehaviour
{
    public Transform target;
    public enum ValueTypes { Rotation, Position }
    public enum FixTypes { Origin, RealTime }
    public enum PivotTypes { Local, Grobal }
    public enum LerpTypes { None, Lerp, Slerp }


    int option;

    public ValueTypes valueType;
    public FixTypes FixType;
    public PivotTypes targetPivotType;
    public PivotTypes applyPivotType;
    public LerpTypes lerpType;


    //public bool fixRotationZero;
    //public bool fixRotationOrgin;
    //public bool PosLerp;
    //public bool fixPositionOrigin;
    private Vector3 OriginPos;
    private Quaternion OriginRot;

    public bool isFixGlobalRotation;
    public Quaternion fixGlobalRotation;

    private void Start()
    {
        if (target != null && FixType == FixTypes.Origin)
        {
            if (valueType == ValueTypes.Position)
            {
                if (targetPivotType == PivotTypes.Local)
                    OriginPos = target.localPosition;
                else
                    OriginPos = target.position;
            }
            else
            {
                if (targetPivotType == PivotTypes.Local)
                    OriginRot = target.localRotation;
                else
                    OriginRot = target.rotation;
            }
        }

        switch (valueType)
        {
            case ValueTypes.Position:
                switch (targetPivotType)
                {
                    case PivotTypes.Grobal:
                        switch (applyPivotType)
                        {
                            case PivotTypes.Grobal:
                                switch (FixType)
                                {
                                    case FixTypes.Origin:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 0;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 1;
                                                break;
                                            default:
                                                option = 2;
                                                break;
                                        }
                                        break;
                                    case FixTypes.RealTime:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 3;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 4;
                                                break;
                                            default:
                                                option = 5;
                                                break;
                                        }
                                        break;
                                }
                                break;
                            case PivotTypes.Local:
                                switch (FixType)
                                {
                                    case FixTypes.Origin:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 6;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 7;
                                                break;
                                            default:
                                                option = 8;
                                                break;
                                        }
                                        break;
                                    case FixTypes.RealTime:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 9;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 10;
                                                break;
                                            default:
                                                option = 11;
                                                break;
                                        }
                                        break;
                                }
                                break;
                        }
                        break;
                    case PivotTypes.Local:
                        switch (applyPivotType)
                        {
                            case PivotTypes.Grobal:
                                switch (FixType)
                                {
                                    case FixTypes.Origin:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 12;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 13;
                                                break;
                                            default:
                                                option = 14;
                                                break;
                                        }
                                        break;
                                    case FixTypes.RealTime:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 15;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 16;
                                                break;
                                            default:
                                                option = 17;
                                                break;
                                        }
                                        break;
                                }
                                break;
                            case PivotTypes.Local:
                                switch (FixType)
                                {
                                    case FixTypes.Origin:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 18;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 19;
                                                break;
                                            default:
                                                option = 20;
                                                break;
                                        }
                                        break;
                                    case FixTypes.RealTime:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 21;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 22;
                                                break;
                                            default:
                                                option = 23;
                                                break;
                                        }
                                        break;
                                }
                                break;
                        }
                        break;
                }
                break;
            case ValueTypes.Rotation:
                switch (targetPivotType)
                {
                    case PivotTypes.Grobal:
                        switch (applyPivotType)
                        {
                            case PivotTypes.Grobal:
                                switch (FixType)
                                {
                                    case FixTypes.Origin:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 24;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 25;
                                                break;
                                            default:
                                                option = 26;
                                                break;
                                        }
                                        break;
                                    case FixTypes.RealTime:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 27;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 28;
                                                break;
                                            default:
                                                option = 29;
                                                break;
                                        }
                                        break;
                                }
                                break;
                            case PivotTypes.Local:
                                switch (FixType)
                                {
                                    case FixTypes.Origin:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 30;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 31;
                                                break;
                                            default:
                                                option = 32;
                                                break;
                                        }
                                        break;
                                    case FixTypes.RealTime:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 33;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 34;
                                                break;
                                            default:
                                                option = 35;
                                                break;
                                        }
                                        break;
                                }
                                break;
                        }
                        break;
                    case PivotTypes.Local:
                        switch (applyPivotType)
                        {
                            case PivotTypes.Grobal:
                                switch (FixType)
                                {
                                    case FixTypes.Origin:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 36;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 37;
                                                break;
                                            default:
                                                option = 38;
                                                break;
                                        }
                                        break;
                                    case FixTypes.RealTime:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 39;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 40;
                                                break;
                                            default:
                                                option = 41;
                                                break;
                                        }
                                        break;
                                }
                                break;
                            case PivotTypes.Local:
                                switch (FixType)
                                {
                                    case FixTypes.Origin:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 42;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 43;
                                                break;
                                            default:
                                                option = 44;
                                                break;
                                        }
                                        break;
                                    case FixTypes.RealTime:
                                        switch (lerpType)
                                        {
                                            case LerpTypes.Lerp:
                                                option = 45;
                                                break;
                                            case LerpTypes.Slerp:
                                                option = 46;
                                                break;
                                            default:
                                                option = 47;
                                                break;
                                        }
                                        break;
                                }
                                break;
                        }
                        break;
                }
                break;
        }
    }
    void FixedUpdate()
    {
        if (isFixGlobalRotation)
            transform.rotation = fixGlobalRotation;
        else
            switch (option)
            {
                //위치
                case 0:
                    transform.position = Vector3.Lerp(transform.position, OriginPos, 5 * Time.deltaTime);
                    break;
                case 1:
                    transform.position = Vector3.Slerp(transform.position, OriginPos, 5 * Time.deltaTime);
                    break;
                case 2:
                    transform.position = OriginPos;
                    break;
                case 3:
                    transform.position = Vector3.Lerp(transform.position, target.position, 5 * Time.deltaTime);
                    break;
                case 4:
                    transform.position = Vector3.Slerp(transform.position, target.position, 5 * Time.deltaTime);
                    break;
                case 5:
                    transform.position = target.position;
                    break;
                case 6:
                    transform.localPosition = Vector3.Lerp(transform.localPosition, OriginPos, 5 * Time.deltaTime);
                    break;
                case 7:
                    transform.localPosition = Vector3.Slerp(transform.localPosition, OriginPos, 5 * Time.deltaTime);
                    break;
                case 8:
                    transform.localPosition = OriginPos;
                    break;
                case 9:
                    transform.localPosition = Vector3.Lerp(transform.localPosition, target.position, 5 * Time.deltaTime);
                    break;
                case 10:
                    transform.localPosition = Vector3.Slerp(transform.localPosition, target.position, 5 * Time.deltaTime);
                    break;
                case 11:
                    transform.localPosition = target.position;
                    break;
                case 12:
                    transform.position = Vector3.Lerp(transform.position, OriginPos, 5 * Time.deltaTime);
                    break;
                case 13:
                    transform.position = Vector3.Slerp(transform.position, OriginPos, 5 * Time.deltaTime);
                    break;
                case 14:
                    transform.position = OriginPos;
                    break;
                case 15:
                    transform.position = Vector3.Lerp(transform.position, target.localPosition, 5 * Time.deltaTime);
                    break;
                case 16:
                    transform.position = Vector3.Slerp(transform.position, target.localPosition, 5 * Time.deltaTime);
                    break;
                case 17:
                    transform.position = target.localPosition;
                    break;
                case 18:
                    transform.localPosition = Vector3.Lerp(transform.localPosition, OriginPos, 5 * Time.deltaTime);
                    break;
                case 19:
                    transform.localPosition = Vector3.Slerp(transform.localPosition, OriginPos, 5 * Time.deltaTime);
                    break;
                case 20:
                    transform.localPosition = OriginPos;
                    break;
                case 21:
                    transform.localPosition = Vector3.Lerp(transform.localPosition, target.localPosition, 5 * Time.deltaTime);
                    break;
                case 22:
                    transform.localPosition = Vector3.Slerp(transform.localPosition, target.localPosition, 5 * Time.deltaTime);
                    break;
                case 23:
                    transform.localPosition = target.localPosition;
                    break;

                // 회전
                case 24:
                    transform.rotation = Quaternion.Lerp(transform.rotation, OriginRot, 5 * Time.deltaTime);
                    break;
                case 25:
                    transform.rotation = Quaternion.Slerp(transform.rotation, OriginRot, 5 * Time.deltaTime);
                    break;
                case 26:
                    transform.rotation = OriginRot;
                    break;
                case 27:
                    transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, 5 * Time.deltaTime);
                    break;
                case 28:
                    transform.rotation = Quaternion.Slerp(transform.rotation, target.rotation, 5 * Time.deltaTime);
                    break;
                case 29:
                    transform.rotation = target.rotation;
                    break;
                case 30:
                    transform.localRotation = Quaternion.Lerp(transform.localRotation, OriginRot, 5 * Time.deltaTime);
                    break;
                case 31:
                    transform.localRotation = Quaternion.Slerp(transform.localRotation, OriginRot, 5 * Time.deltaTime);
                    break;
                case 32:
                    transform.localRotation = OriginRot;
                    break;
                case 33:
                    transform.localRotation = Quaternion.Lerp(transform.localRotation, target.rotation, 5 * Time.deltaTime);
                    break;
                case 34:
                    transform.localRotation = Quaternion.Slerp(transform.localRotation, target.rotation, 5 * Time.deltaTime);
                    break;
                case 35:
                    transform.localRotation = target.rotation;
                    break;
                case 36:
                    transform.rotation = Quaternion.Lerp(transform.rotation, OriginRot, 5 * Time.deltaTime);
                    break;
                case 37:
                    transform.rotation = Quaternion.Slerp(transform.rotation, OriginRot, 5 * Time.deltaTime);
                    break;
                case 38:
                    transform.rotation = OriginRot;
                    break;
                case 39:
                    transform.rotation = Quaternion.Lerp(transform.rotation, target.localRotation, 5 * Time.deltaTime);
                    break;
                case 40:
                    transform.rotation = Quaternion.Slerp(transform.rotation, target.localRotation, 5 * Time.deltaTime);
                    break;
                case 41:
                    transform.rotation = target.localRotation;
                    break;
                case 42:
                    transform.localRotation = Quaternion.Lerp(transform.localRotation, OriginRot, 5 * Time.deltaTime);
                    break;
                case 43:
                    transform.localRotation = Quaternion.Slerp(transform.localRotation, OriginRot, 5 * Time.deltaTime);
                    break;
                case 44:
                    transform.localRotation = OriginRot;
                    break;
                case 45:
                    transform.localRotation = Quaternion.Lerp(transform.localRotation, target.localRotation, 5 * Time.deltaTime);
                    break;
                case 46:
                    transform.localRotation = Quaternion.Slerp(transform.localRotation, target.localRotation, 5 * Time.deltaTime);
                    break;
                case 47:
                    transform.localRotation = target.localRotation;
                    break;
            }
        //if (fixPositionOrigin)
        //    if (PosLerp)
        //        transform.localPosition = Vector3.Lerp(transform.localPosition, OriginPos, 5 * Time.deltaTime);
        //    else
        //        transform.position = target.transform.position;
        //if (fixRotationZero)
        //    if (rotationPivot == PivotTypes.Local)
        //        transform.localRotation = new Quaternion(0, 0, 0, 1);
        //    else
        //        transform.rotation = new Quaternion(0, 0, 0, 1);
        //if (fixRotationOrgin)
        //    transform.rotation = OriginRot;
    }
}
