﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ElectricCircuits : ElectricDevice
{
    public List<Vector2> receivedLines = new List<Vector2>();
    public Vector2[] lines = new Vector2[4];

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        for (int i = 0; i < lines.Length; i++)
        {
            lines[0] = new Vector2(0, 1).normalized;
            lines[1] = new Vector2(0, -1).normalized;
            lines[2] = new Vector2(-1, 0).normalized;
            lines[3] = new Vector2(1, 0).normalized;
        }
    }

    public bool checkElectricDir(Vector2 vector2)
    {
        
        for (int i=0; i<lines.Length;i++)
        {
            if (vector2.normalized == -lines[i].normalized)
            {
                receivedLines.Add(lines[i]);
                return true;
            }
        }
        return false;
    }

    public void OnElectric(Vector3 vector3, bool value)
    {
        Vector2 reciveDir = vector3;
        if (checkElectricDir(reciveDir))
            on = value;
    }
    public void PassElectric()
    {
        int layerMask = (1 << 11);
        for (int i = 0; i < lines.Length; i++)
        {
            if (receivedLines.Contains(lines[i]))
                continue;

            Vector2 pos = transform.position;
            RaycastHit2D hit = Physics2D.Raycast(pos + lines[i].normalized * 0.16f, lines[i], 0.05f, layerMask);
            if (hit && hit.collider.gameObject != gameObject)
            {
                if (hit.collider.GetComponent<ElectricCircuits>() != null)
                {
                    //Quaternion qRotate = Quaternion.AngleAxis(transform.rotation.eulerAngles.z, Vector3.forward);
                    //Vector2 ErectricDir = qRotate * -transform.up;
                    ElectricCircuits line = hit.collider.GetComponent<ElectricCircuits>();
                    line.OnElectric(lines[i], on);
                    
                }
                else if (hit.collider.GetComponent<ElectricDevice>() != null)
                {
                    ElectricDevice device = hit.collider.GetComponent<ElectricDevice>();
                    Vector2 devicePlug = device.transform.up.normalized;
                    if (devicePlug == lines[i].normalized)
                        device.on = on;
                }
            }
        }
    }
    protected override void WakeUp()
    {
        PassElectric();
        base.WakeUp();
    }
    protected override void ShotDown()
    {
        PassElectric();
        base.ShotDown();
        receivedLines.Clear();
    }
}
