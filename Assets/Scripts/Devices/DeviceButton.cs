﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceButton : ElectricDevice
{
    public ElectricLineCheck lineChecker;
    public override void Start()
    {
        base.Start();
    }
    // Start is called before the first frame update
    protected override void WakeUp()
    {
        base.WakeUp();
        if(lineChecker.line != null)
            lineChecker.line.OnElectric(-transform.up,  true);
    }
    protected override void ShotDown()
    {
        base.ShotDown();
        if (lineChecker.line != null)
            lineChecker.line.OnElectric(-transform.up, false);
    }
}
