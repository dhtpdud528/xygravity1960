﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CubeGenerator : ElectricDevice
{
    public GameObject spawnObj;
    public GameObject spawnParticle;

    GameObject spawnedObj;
    public void Update()
    {
        if(on)
            if(spawnedObj == null)
                WakeUp();
    }
    protected override void WakeUp()
    {
        base.WakeUp();
        if (spawnedObj != null)
        {
            if (spawnedObj.GetComponent<Rigidbody2D>() != null)
                gameManager.allOBJ.Remove(spawnedObj.GetComponent<Rigidbody2D>());
            if (spawnedObj.active && spawnedObj.GetComponent<G_Object>() != null)
                spawnedObj.GetComponent<G_Object>().BreakParticles();
            Destroy(spawnedObj);
            spawnedObj = null;
        }
        StartCoroutine(SpawnOBJ());
        //Instantiate(spawnParticle, transform.position, transform.localRotation);
    }
    IEnumerator SpawnOBJ()
    {
        spawnedObj = Instantiate(spawnObj);
        spawnedObj.SetActive(false);
        yield return new WaitForSeconds(1f);
        if (spawnedObj.GetComponent<Rigidbody2D>() != null)
        {
            spawnedObj.GetComponent<Rigidbody2D>().velocity *= 0;
            spawnedObj.GetComponent<Rigidbody2D>().angularVelocity *= 0;
            gameManager.allOBJ.Add(spawnedObj.GetComponent<Rigidbody2D>());
        }
        spawnedObj.SetActive(true);
        spawnedObj.transform.SetParent(gameManager.DefaultTilemap.transform);
        spawnedObj.transform.position = transform.position;
        
    }
}
