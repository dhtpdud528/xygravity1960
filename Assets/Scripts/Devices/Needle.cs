﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Needle : MonoBehaviour
{
    private float limit = 3;
    public void Awake()
    {
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.isTrigger) return;
        if (collision.collider.GetComponent<PlayerController>())
            collision.collider.GetComponent<PlayerController>().Die();
        else if (collision.relativeVelocity.magnitude > limit)
            Instantiate(GameManager.instance.objDetstoyParticles, collision.contacts[0].point, Quaternion.identity);
    }
}
