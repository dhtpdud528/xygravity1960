﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricLineCheck : MonoBehaviour
{
    public ElectricCircuits line;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnTriggerStay2D(Collider2D collision)
    {

        if (collision.gameObject.GetComponent<ElectricCircuits>() != null)
            line = collision.gameObject.GetComponent<ElectricCircuits>();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<ElectricCircuits>() != null)
        {
            collision.gameObject.GetComponent<ElectricCircuits>().OnElectric(-transform.up, false);
            line = null;
        }
    }
}
