﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;

public class DeviceRoomGravityController : ElectricDevice
{
    public Vector2 direction;
    public Transform gravityVector;

    public override void Start()
    {
        base.Start();
        gravityVector = transform.GetChild(0);
        float angle = Vector2.Angle(gravityVector.up.normalized,  direction.normalized);
        Quaternion quart = Quaternion.AngleAxis(angle, Vector3.forward);
        gravityVector.rotation = quart;
    }
    protected override void WakeUp()
    {
        base.WakeUp();
        gameManager.gravityDir += new Vector2(direction.x, direction.y).normalized * 9.81f;
        gameManager.gravityZone.directionX = gameManager.gravityDir.x;
        gameManager.gravityZone.directionY = gameManager.gravityDir.y;
    }
    protected override void Running()
    {
        base.Running();
        Vector2 gravity = direction * 9.81f;
        for (int i = 0; i < gameManager.allOBJ.Count; i++)
        {
            if (gameManager.allOBJ[i] == null) continue;
            gameManager.allOBJ[i].velocity += gravity * Time.deltaTime;
        }
    }
    protected override void ShotDown()
    {
        base.ShotDown();
        gameManager.gravityDir -= new Vector2(direction.x, direction.y).normalized * 9.81f;
        gameManager.gravityZone.directionX = gameManager.gravityDir.x;
        gameManager.gravityZone.directionY = gameManager.gravityDir.y;
    }
    public static float GetAngle(Vector2 pos1, Vector2 pos2)

    {

        Vector2 dir = (pos2 - pos1).normalized;

        float angle = Mathf.Atan2(dir.x, dir.y);

        float degree = (angle * 180) / Mathf.PI;

        if (degree < 0)

            degree += 360;



        float reDegree = 360 - degree;



        return reDegree;

    }
}
