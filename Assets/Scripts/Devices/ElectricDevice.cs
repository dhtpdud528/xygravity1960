﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricDevice : MonoBehaviour
{
    public bool on;
    public AudioClip wakeUpSound;
    public AudioClip shutDownSound;
    public Animator anim;
    protected GameManager gameManager;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    // Start is called before the first frame update
    public virtual void Start()
    {
        gameManager = GameManager.instance;
        setSprite();
        if(on)
            WakeUp();
        else
            ShotDown();
        //StartCoroutine(CheckElectric());
    }
    public void setSprite()
    {
        if (anim != null)
            if (on)
                anim.SetBool("on", true);
            else
                anim.SetBool("on", false);
    }
    IEnumerator CheckElectric()
    {
        switch (on)
        {
            case false:
                while (!on)
                {
                    Sleep();
                    yield return new WaitForEndOfFrame();
                }
                WakeUp(); break;

            case true:
                while (on)
                {
                    Running();
                    yield return new WaitForEndOfFrame();
                }
                ShotDown(); break;
        }
    }
    protected virtual void Sleep()
    {

    }
    protected virtual void WakeUp()
    {
        if(wakeUpSound != null)
            gameManager.audioSource.PlayOneShot(wakeUpSound);
        setSprite();
        StartCoroutine(CheckElectric());
    }
    protected virtual void Running()
    {

    }
    protected virtual void ShotDown()
    {
        if (shutDownSound != null)
            gameManager.audioSource.PlayOneShot(shutDownSound);
        setSprite();
        StartCoroutine(CheckElectric());
    }
}
