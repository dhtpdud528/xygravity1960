﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityZone : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D collision)
    {
        Vector2 down = -transform.up;
        collision.attachedRigidbody.velocity += 9.18f * down;
    }
}
