﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class EventsList : MonoBehaviour
{
    public UnityEvent StartEvent;
    public GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void FadeIn()
    {
        GameManager.instance.fade.DOColor(new Color(0, 0, 0, 1), 5);
    }
    public void Fadeout()
    {
        GameManager.instance.fade.DOColor(new Color(0, 0, 0, 0), 5);
    }
}
