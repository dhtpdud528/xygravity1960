﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DevicePusher : ElectricDevice
{
    public AudioClip pulse;
    public float power;
    public Collider2D pushOBJ;
    // Update is called once per frame
    protected override void WakeUp()
    {
        base.WakeUp();
        if (pushOBJ != null)
        {
            Vector2 up = transform.up.normalized;
            pushOBJ.attachedRigidbody.angularVelocity *= 0;
            pushOBJ.attachedRigidbody.velocity += up * 3;
        }
    }
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(on)
            collision.sharedMaterial = gameManager.ZeroFriction;
        if (collision.GetComponent<G_Object>()) return;
        if (on && collision.attachedRigidbody != null && collision.attachedRigidbody.bodyType == RigidbodyType2D.Dynamic)
        {
            Vector2 up = transform.up.normalized;
            Vector2 toColiision = collision.attachedRigidbody.velocity;

            Quaternion qRotate = Quaternion.AngleAxis(-transform.rotation.eulerAngles.z, Vector3.forward);
            Vector2 v3Dest = qRotate * toColiision;
            Vector2 temp = new Vector2(v3Dest.x, -1* v3Dest.y);
            qRotate = Quaternion.AngleAxis(transform.rotation.eulerAngles.z, Vector3.forward);
            temp = qRotate * temp;

            collision.attachedRigidbody.velocity = temp;
            collision.attachedRigidbody.velocity *= new Vector2(1, power);
            if (collision.GetComponent<G_Object>())
            {
                collision.transform.rotation = transform.rotation;
                collision.attachedRigidbody.angularVelocity *= 0;
            }
            anim.CrossFadeInFixedTime("PusherPulse", 0.01f);
            gameManager.audioSource.PlayOneShot(pulse);
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        pushOBJ = collision;
        if (collision.GetComponent<G_Object>() != null || collision.GetComponent<PlayerController>() != null) return;
        Vector2 up = transform.up.normalized;
        if (collision.attachedRigidbody != null && collision.attachedRigidbody.bodyType == RigidbodyType2D.Dynamic)
            if (on)
                collision.attachedRigidbody.velocity += up * 20 * Time.deltaTime;
            else
                collision.attachedRigidbody.velocity += -up * 1 * Time.deltaTime;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        collision.sharedMaterial = gameManager.DefaultFriction;
        pushOBJ = null;
        if (collision.GetComponent<PlayerController>() != null)
            gameManager.playerController.isFloor = false;
    }
}
