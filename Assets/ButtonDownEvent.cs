﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ButtonDownEvent : MonoBehaviour, IPointerDownHandler
{
    public UnityEvent events;
    
    public void OnPointerDown(PointerEventData eventData)
    {
        events.Invoke();
        GameManager.instance.isMouseOverUI = true;
        GameManager.instance.playerCam.preMoseOverUI = true;
    }
}
