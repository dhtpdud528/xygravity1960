﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class NarrationManager : MonoBehaviour
{
    //public List<string> narrations = new List<string>();
    GameManager gameManager;
    public AudioClip pop;
    public Image npcImage;
    public GameObject textbox;
    public Text textboxText;
    public Text nextText;

    public Text tutText;

    Tweener doing;
    public bool canClick;
    public bool isClickScreen;

    public GameObject[] tutOBJs;


    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.instance;
        if(tutOBJs != null)
            for(int i =0; i < tutOBJs.Length; i++)
                if(tutOBJs[i].GetComponent<Rigidbody2D>())
                    gameManager.allOBJ.Add(tutOBJs[i].GetComponent<Rigidbody2D>());
    }

    // Update is called once per frame
    void Update()
    {
        if (canClick && doing != null && Input.GetMouseButtonDown(0) && !gameManager.IsJoyStick())
        {
            if (doing.IsPlaying())
                doing.Complete();
            else
                isClickScreen = true;
        }
    }
    public bool CheckIsClickScreen()
    {
        if (isClickScreen)
        {
            isClickScreen = false;
            return true;
        }
        else
            return false;
    }
    public void Narration(string title)
    {
        switch(title)
        {
            case "TUT1": StartCoroutine(Narration1Coroutine()); break;
            case "TUT2": StartCoroutine(Narration2Coroutine()); break;
        }
    }
    public void POPTextbox(bool val)
    {
        if (pop != null && val)
            gameManager.audioSource.PlayOneShot(pop);
        textbox.gameObject.SetActive(val);
    }
    IEnumerator Narration2Coroutine()
    {
        setClickable(false);
        yield return new WaitForSeconds(5f);
        textboxText.text = "";
        POPTextbox(true);

        yield return new WaitForSeconds(2f);
        setClickable(true);
        DoNarration(true, "이번 문제부터는 \"<b>질량 수정</b>\" 능력이 활성화됩니다.", 2f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "귀하는 지금부터 해당 능력을 이용하여, 저희 HSL에서 제공하는 물체에 한해서, 원하시는 물체의 질량을 마음대로 수정하실 수 있습니다.", 4f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "추가 질량을 가진 물체들은 별다른 부피의 변화없이 \"<b>지구하나 만큼의 중력</b>\"과 동등한 중력장을 발산하게 됩니다,", 3f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "해당 중력장의 발산 범위 또한 원하시는 대로 수정이 가능합니다.", 2f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "먼저 방금 제공된 큐브의 질량을 수정해 보시기 바랍니다.", 2f);
        tutOBJs[0].SetActive(true);
        Instantiate(gameManager.objDetstoyParticles, tutOBJs[0].transform);
        yield return new WaitUntil(() => CheckIsClickScreen());

        bool nextTut = false;
        while (!nextTut)
        {
            setClickable(false);
            POPTextbox(false);
            gameManager.canClickOBJ = true;
            tutText.text = "큐브를 누르고 드래그해서 큐브의 중력장을 바꾸어 보세요!" +
            "\n\n화면 기준" +
            "\n우측드래그 \"<color=#FF936B><b>인력</b></color>\"" +
            "\n좌측드래그 \"<color=#6B93FF><b>척력</b></color>\"";
            yield return new WaitUntil(() => gameManager.playerCam.isDotMod);
            tutText.text = "큐브를 누르면 큐브의 중앙에서 중력장의 크기를 조절할 수 있는 \"<b>선</b>\"이 보이게 됩니다." +
                "\n\n화면 기준" +
                "\n우측드래그 \"<color=#FF936B><b>인력</b></color>\"" +
                "\n좌측드래그 \"<color=#6B93FF><b>척력</b></color>\"";
            yield return new WaitUntil(() => !gameManager.playerCam.isDotMod);
            tutText.text = "해당 선의 길이가 일정 길이 이상이 되면, 선의 색상이 \"<color=#FF936B><b>인력</b></color>\" 또는  \"<color=#6B93FF><b>척력</b></color>\"의 색으로 고정됩니다.";
            yield return new WaitUntil(() => !gameManager.playerCam.isMassMod);
            tutText.text = "";

            setClickable(true);
            POPTextbox(true);

            gameManager.moveJoystick.gameObject.SetActive(false);
            gameManager.handJoystick.gameObject.SetActive(false);
            DoNarration(true, "잘 하셨습니다.", 1f);
            yield return new WaitUntil(() => CheckIsClickScreen());
            DoNarration(true, "<color=#B50000>마지막으로, 중력장이 수정된 물체는 집을 수 없다는점 명심해 주세요.</color>", 2f);
            yield return new WaitUntil(() => CheckIsClickScreen());
            DoNarration(true, "다시 듣기를 원하신다면 \"좌측 조이스틱\"을, 확실히 이해했고 다음으로 넘어가길 원하신다면 \"우측 조이스틱\"을 터치해 주세요.", 3f);
            yield return new WaitForSeconds(3f);
            gameManager.moveJoystick.gameObject.SetActive(true);
            gameManager.handJoystick.gameObject.SetActive(true);

            while (true)
            {
                if (gameManager.handJoystick.Vertical > 0 || gameManager.handJoystick.Horizontal > 0)
                {
                    nextTut = true;
                    break;
                }
                else if (gameManager.moveJoystick.Vertical > 0 || gameManager.moveJoystick.Horizontal > 0)
                {
                    nextTut = false;
                    break;
                }
                yield return new WaitForEndOfFrame();
            }
        }
        isClickScreen = false;
        setClickable(true);
        POPTextbox(true);
        DoNarration(true, "해당 능력은 좌측 상단에 \"<b>일시정지 버튼</b>\"을 누른 후에도 사용할 수 있습니다.", 2f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "그리고 그 옆의 \"<b>재시작</b>\"을 누르게 되면 해당 방을 처음부터 다시 시작 할 수 있습니다.", 2f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "저의 역할은 여기 까지 입니다.", 1f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "기초교육수료를 진심으로 축하드립니다.", 1f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "해당 교육이후 실험실에서 안전부주의로 발생하게되는 모든 일에대한 책임은 본인에게 있으며,", 1f);
        yield return new WaitForSeconds(1f);
        DoNarration(true, "앞으로 귀하께서 진행하며 얻게되는 모든 실험결과들은 국가와 저희 HSL의 소중한 자산이 될것입니다.", 2f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "감사합니다.", 2f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        POPTextbox(false);
        tutOBJs[1].SetActive(true);
        tutOBJs[2].SetActive(true);
        Instantiate(gameManager.objDetstoyParticles, tutOBJs[1].transform);
    }
    IEnumerator Narration1Coroutine()
    {
        setClickable(false);
        yield return new WaitForSeconds(3f);
        textboxText.text = "";
        POPTextbox(true);
        
        yield return new WaitForSeconds(2f);
        setClickable(true);
        DoNarration(true, "안녕하세요.", 1f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "Hallym Science Laboratory에 오신것을 환영합니다.", 2f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "먼저 해당 실험에 자원해 주신 귀하와, 아낌없이 저희 \"HSL\"에 기부해주신 귀하의 신체에 대해 감사인사를 전합니다.", 3f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "귀하께서는 저희 HSL의 수 많은 실험중 \"<b>다중력 공간 해석 능력 평가실험</b>\"에 배치되셨습니다.", 3f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "현재 귀하께서 위치하신 곳은 실험전 사전 교육을 받기위한 교육대기실이며,", 2f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "방 내부의 무중력 상태는 이상현상이나 긴급상황이아닌 정상적인 현상이므로 당황하지 마시고, 차분하게 대기해 주시기 바랍니다.", 4f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, Texts("저는 귀하에게 이번 실험을 안내해드릴,"), 2f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        setClickable(false);
        DoNarration(false, Texts("\"<b>영희</b>\""), 1f);
        npcImage.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        DoNarration(false, Texts("입니다."), 0.1f);
        setClickable(true);
        yield return new WaitUntil(() => CheckIsClickScreen());

        yield return new WaitForSeconds(1f);

        setClickable(false);
        DoNarration(true, "곧 가상 중력이 활성화됩니다.", 1f);
        yield return new WaitForSeconds(2f);
        DoNarration(true, Texts("3 ", "2 ", "1 "), 1,1,1);
        yield return new WaitForSeconds(3f);
        tutOBJs[0].gameObject.SetActive(true);
        Instantiate(gameManager.objDetstoyParticles, tutOBJs[0].transform);
        yield return new WaitForSeconds(2f);
        

        setClickable(true);
        DoNarration(true, "실험 내용을 설명드리기 앞서, 간단한 운동능력 평가를 실시하겠습니다.", 2f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "현재 신체의 균형을 잡기 힘든것은, 오랜 시간 신체가 무중력에 노출되어 발생되는 정상적인 신체반응입니다.", 3f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "갑자기 일어나려고 하지 마시고, 바닥을 집고 천천히 일어나시기 바랍니다.", 2f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        POPTextbox(false);
        setClickable(false);

        tutText.text = "캐릭터를 눌러 바닥쪽으로 드래그 해서 일어나세요!\n드래그 방향이 곧 캐릭터의 바닥이 됩니다.";
        yield return new WaitUntil(() => !gameManager.playerController.isSettingGyro && gameManager.playerController.isFloor);
        tutText.text = "";

        setClickable(true);
        POPTextbox(true);
        DoNarration(true, "잘하셨습니다.", 1f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "다음은 천천히 앞으로 걸어 보시기 바랍니다.", 2f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        POPTextbox(false);
        setClickable(false);

        gameManager.moveJoystick.gameObject.SetActive(true);
        gameManager.buttonJump.gameObject.SetActive(true);
        tutText.text = "화면 좌측의 조이스틱과 우측의 점프버튼으로 플레이어를 조작할 수 있습니다.";
        yield return new WaitUntil(() => !gameManager.IsJoyStick());
        yield return new WaitForSeconds(6f);
        tutText.text = "";

        setClickable(true);
        POPTextbox(true);
        DoNarration(true, "훌륭합니다!", 1f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "귀하의 신체는 앞으로의 모든 테스트를 정상적으로 수행할 수 있음이 확인되었습니다.", 3f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "이제 구체적인 실험내용과 사전교육내용을 설명드리겠습니다.", 2f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "저희 HSL의 중력공간해석실험은 문제풀이 형식으로 진행되며, 각 문제당 방 하나의 공간이 제공됩니다.", 3f);
        yield return new WaitUntil(() => CheckIsClickScreen());

        DoNarration(true, "각 방 마다 다음 문제로 넘어갈 \"<b>출구</b>\" 하나와", 2f);
        tutOBJs[1].gameObject.SetActive(true);
        Instantiate(gameManager.objDetstoyParticles, tutOBJs[1].transform);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(false, "\n 출구를 작동시킬 \"<b>버튼</b>\" 하나,", 2f);
        tutOBJs[2].gameObject.SetActive(true);
        Instantiate(gameManager.objDetstoyParticles, tutOBJs[2].transform);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(false, "\n 그 버튼을 누를 \"<b>큐브</b>\"또는 \"<b>볼</b>\" 하나 이상이 기본적으로 제공됩니다.", 2f);
        tutOBJs[3].gameObject.SetActive(true);
        Instantiate(gameManager.objDetstoyParticles, tutOBJs[3].transform);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "출구는 버튼이 눌리는동안 개방되며, 버튼이 눌리지 않는다면 문은 곧바로 닫히게됩니다.", 3f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "이외에도 버튼으로 작동하는 여러 기기장치들이 있습니다.", 2f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "각각 제공되는 기기장치들과 방의 구조를 잘 파악하여, 문제를 해결하시기 바랍니다.", 3f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "문제 풀이시간은 무제한입니다.", 1f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        DoNarration(true, "이제 큐브를 버튼위에 올려서 버튼을 누르고, 출구를 열어 다음 방으로 이동해 주세요.", 3f);
        yield return new WaitUntil(() => CheckIsClickScreen());
        POPTextbox(false);

        gameManager.handJoystick.gameObject.SetActive(true);
        tutText.text = "큐브에 가까이 다가가 화면 우측의 조이스틱으로 큐브를 집으세요.";
        yield return new WaitUntil(() => gameManager.playerController.handOBJ != null);
        tutText.text = "조이스틱을 가장자리까지 움직여 놓을경우, 큐브를 집은상태가 고정됩니다.\n 반대로 큐브를 내려놓고 싶다면, 조이스틱을 중간에서 놓으세요.";

    }
    public void setClickable(bool val)
    {
        canClick = val;
        nextText.enabled = val;
    }
    public string[] Texts(params string[] texts)
    {
        return texts;
    }
    public void DoNarration(bool init, string[] texts, params float[] times)
    {
        if(init)
            textboxText.text = "";
        StartCoroutine(RelativeText(texts, times));
    }
    IEnumerator RelativeText(string[] texts, params float[] times)
    {
        for (int i = 0; i < texts.Length; i++)
        {
            doing = textboxText.DOText(texts[i], times[i]).SetRelative(true);
            yield return new WaitUntil(() => !doing.IsPlaying());
        }
    }
    public void DoNarration(bool init, string texts, float times)
    {
        if (init)
            textboxText.text = "";
        doing = textboxText.DOText(texts, times).SetRelative(!init);
    }
}
