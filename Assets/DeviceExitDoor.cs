﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceExitDoor : ElectricDevice
{
    public int moveScene;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(on && collision.GetComponent<PlayerController>() != null)
            gameManager.MoveNextScene(moveScene);
    }
}
